import requests
import sys
import json
import client.actions
import client.api
from optparse import OptionParser

# parse arguments and check for completeness
parser = OptionParser()
parser.add_option("-u", "--url", dest="url",
                  help="the API base url to use")
parser.add_option("-t", "--token",
                  dest="token",
                  help="the token to access the Api")
(options, args) = parser.parse_args()
if not (options.url and options.token):
    parser.print_help()
    sys.exit(1)

actions = client.actions.Actions(options.url, options.token)
actions.get()
actions.put()
sys.exit
