import requests
import json

class Api:
    url = ""
    token = ""
    headers = ""
    example_uri = "/info.0.json"
    def __init__(self,url,token):
        self.url = url 
        self.token = token
        self.headers = {'authorization': f'Token {token}', 'Content-Type': 'application/json;charset=utf-8'}
    def get(self,uri):
        response = requests.get(self.url + uri, headers=self.headers)
        return(response)
    def put(self, uri, data):
        response = requests.put(self.url + uri, headers=self.headers, data=data)
        return(response.status_code)
    def post(self, uri, data):
        response = requests.post(self.url + uri, headers=self.headers, data=data)
        return(response.status_code)
    def json(self, data):
        return(json.dumps(data.json(), sort_keys=True, indent=2))
    def get_example(self):
        r = self.get(self.example_uri)
        example_json = self.json(r)
        return(example_json)
    def put_example(self, example_json):
        r = self.put(self.example_uri, example_json)
        return(r)
