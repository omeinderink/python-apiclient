import os
import json
import requests
import client.api

class Actions:
    url = ""
    token = ""
    api = client.api.Api
    directory = "/tmp"
    example_filename = "example.json"
    def __init__(self,url,token):
        self.url = url 
        self.token = token
        self.api = client.api.Api(url,token)
    def write_to_disk(self,filename,content):
        os.makedirs(self.directory, exist_ok=True)
        with open(self.directory+"/"+filename, 'w') as file:
            file.writelines(content)
        return(self.directory+filename)
    def read_from_disk(self,filename):
        with open(self.directory+"/"+filename, 'r') as file:
            content=file.read()
        return(content)
    def get(self):
        example = self.api.get_example()
        self.write_to_disk(self.example_filename,example)
        return "successfully written"
    def put(self):
        example = self.read_from_disk(self.example_filename)
        self.api.put_example(example)
        return("successfully put")
